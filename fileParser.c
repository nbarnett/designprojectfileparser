/* fileParser.c
 * Author: Nick Barnett 7/8/15
 * Reads a song text file as specified in the COMP3601 Music Spec
 * The file is parsed and converted to the data format required to 
 * be downloaded to the Nexys board for playback
 *
 * The file name should be specified in the arguments of the program
 * e.g. ./fileParse OdeToJoy.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "DynamicByteArray.h"
//#include "winfunc.h"

#define TRUE  (1 == 1)
#define FALSE (1 == 0)
#define MAX_STR_LEN 15
#define ERROR (-1)
#define DEFAULT_BPM 120
#define NORMAL_STYLE 0
#define SLURRED_STYLE 1
#define STACCATO_STYLE 2

typedef enum {UNDEFINED, BPM, STYLE, NOTE} mode;

mode convert(char str[], DynamicByteArray d);
char noteValue(char *note);
char durationValue(char *dur);
int parseFile(char *inputFile, byte **binaryData);

// Returns the length of the byte array pointed to by binaryData
// ensure to free binaryDate after use
int parseFile(char *inputFile, byte **binaryData) {
    FILE *fp;
    fp = fopen(inputFile, "r");
    
    DynamicByteArray d = createDynamicByteArray();
    
    int hasBeenNote = FALSE;
    int hasBeenBPM = FALSE;
    int hasBeenStyle = FALSE;
    mode strMode;
    
    if (fp == 0) {
        perror("Error");
        disposeDynamicByteArray(d);
        return ERROR;
    } else {
        int x;
        char str[MAX_STR_LEN];
        int i = 0;
        str[i] = '\0';
        int isComment = FALSE;
        
        while ((x = fgetc(fp)) != EOF) {
            if (x == '%') {
                isComment = TRUE;
            }
            if (!isComment) {
                str[i] = x;
                i++;
            }
            if (x == '\n' && i > 0) {
                isComment = FALSE;
                i--;
                str[i] = '\0';
                i = 0;
                if (str[i]) {
                    strMode = convert(str, d);
                    if (strMode == BPM) {
                        if (hasBeenBPM || hasBeenNote) {
                            // Syntax Error
                            printf("BPM Syntax Error\n");
                        } else if (hasBeenStyle) {
                            removeLast(d);
                            insertByte(d, DEFAULT_BPM, 0);
                        }
                        hasBeenBPM = TRUE;
                    } else if (strMode == STYLE) {
                        if (hasBeenStyle || hasBeenNote) {
                            // Syntax Error
                            printf("Style Syntax Error\n");
                        } 
                        hasBeenStyle = TRUE;
                    } else if (strMode == NOTE) {
                        if (!hasBeenStyle) {
                            // Insert normal style at start
                            insertByte(d, NORMAL_STYLE, 0);
                            hasBeenStyle = TRUE;
                        }
                        if (!hasBeenBPM) {
                            // Insert default BPM at start
                            insertByte(d, DEFAULT_BPM, 0);
                            hasBeenBPM = TRUE;
                        }
                        hasBeenNote = TRUE;
                    }
                }
            } else if (x == '\n' && i == 0) {
                isComment = FALSE;
            }  
        }
    }
    
    fclose(fp);
    addByte(d, 0xFF);
    
    *binaryData = convertToByteArray(d);
    int size = getNumElements(d);
    
    disposeDynamicByteArray(d);
    
    return size;
}

mode convert(char str[], DynamicByteArray d) {

    byte b;

    int note = 0;
    int duration = 0;
    
    char *token;
    mode strMode = UNDEFINED;
    
    token = strtok(str, " ");

    if (!stricmp(token, "BPM")) {
        strMode = BPM;
    } else if (!stricmp(token, "NORMAL")) {
        strMode = STYLE;
        //printf("style = normal\n");
        b = NORMAL_STYLE;
    } else if (!stricmp(token, "SLURRED")) {
        strMode = STYLE;
        //printf("style = slurred\n");
        b = SLURRED_STYLE;
    } else if (!stricmp(token, "STACCATO")) {
        strMode = STYLE;
        //printf("style = staccato\n");
        b = STACCATO_STYLE;
    } else {
        strMode = NOTE;
        if (!stricmp(token, "REST")) {
            note = 0;
        } else {
            note = noteValue(token);
        }
    }
    
    token = strtok(NULL, " ");
    
    if (strMode == STYLE) {
        addByte(d, b);
        if (token != NULL) {
             printf("Invalid style syntax\n");
        }
    } else if (strMode == BPM) {
        int bpm = atoi(token);
        assert(bpm >= 60 && bpm <= 200);
        //printf("BPM = %d\n", bpm); 
        b = bpm;
        addByte(d, b);
    } else if (strMode == NOTE) {
        duration = durationValue(token);
        //printf("Pitch: %d, Duration: %d\n", note, duration);
        b = note;
        addByte(d, b);
        b = duration;
        addByte(d, b);
    }
    
    if (strtok(NULL, " ") != NULL) {
        perror("Too many arguments\n");
    }
    return 0;
}

char noteValue(char *note) {
    int length = strlen(note);
    char noteName = 0;
    if (length < 2) {
        printf("Invalid Syntax\n");
        return 0;
    } else {
        noteName = note[0];
        noteName = tolower(noteName);
        assert(noteName <= 'g');
        noteName -= 'c';
        if (noteName < 0) noteName = (noteName + 7);
        noteName *= 2;
        if (noteName <= 4) noteName++;
        
        int octave = atoi(note + 1);
        assert(octave >= 3 && octave <= 6);
        noteName += 12 * (octave - 3);
        if (length > 2) {
            char mod = note[2];
            if (mod == '#') noteName++;
            if (mod == 'b') noteName--;
        } 
    }
    return noteName;
}

char durationValue(char *dur) {
    char duration = 4;
    
    if (!stricmp(dur, "sq")) {
        duration = 0;
    } else if (!stricmp(dur, "t")) {
        duration = 1;
    } else if (!stricmp(dur, "q")) {
        duration = 2;
    } else if (!stricmp(dur, "dq")) {
        duration = 3;
    } else if (!stricmp(dur, "c")) {
        duration = 4;
    } else if (!stricmp(dur, "dc")) {
        duration = 5;
    } else if (!stricmp(dur, "m")) {
        duration = 6;
    } else if (!stricmp(dur, "dm")) {
        duration = 7;
    } else if (!stricmp(dur, "sb")) {
        duration = 8;
    } else if (!stricmp(dur, "b")) {
        duration = 9;
    }
    
    return duration;
}


