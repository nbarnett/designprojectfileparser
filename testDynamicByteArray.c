#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "DynamicByteArray.h"

void testAddByte(void);
void testConvert(void);

int main(int argc, char *argv[]) {
    printf("Testing Dynamic Byte Array ADT interface function\n\n");
    
    testAddByte();
    testConvert();
    
    printf("\nAll tests passed. You are Awesome!\n");
}

void testAddByte(void) {
    printf("*** Testing creation ***\n");
    DynamicByteArray d = createDynamicByteArray();
    
    assert(d != NULL);
    assert(getNumElements(d) == 0);
    
    printf("*** Testing add byte ***\n");
    byte b = 't';
    addByte(d, b);
    assert(getNumElements(d) == 1);
    b = 'e';
    addByte(d, b);
    assert(getNumElements(d) == 2);
    b = 's';
    addByte(d, b);
    assert(getNumElements(d) == 3);
    b = 't';
    addByte(d, b);
    assert(getNumElements(d) == 4);
    
    disposeDynamicByteArray(d);
}

void testConvert(void) {
    printf("*** Testing Byte Array Conversion ***\n");
    
    DynamicByteArray d = createDynamicByteArray();
    addByte(d, 't');    
    addByte(d, 'e');
    addByte(d, 's');
    addByte(d, 't');
    assert(getNumElements(d) == 4);
    printDynamicByteArray(d);
    
    byte *b = convertToByteArray(d);
    //int size = getNumElements(d);
    
    assert(b[0] == 't');
    assert(b[1] == 'e');
    assert(b[2] == 's');
    assert(b[3] == 't');
    
    free(b);
    disposeDynamicByteArray(d);
}