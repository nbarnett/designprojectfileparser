import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;

import au.edu.unsw.cse.swing.Gui;

public class MusicPlayerViewController {
	MainGUI gui;
	static File openFile;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Button Clicked");
			if (e.getSource().getClass().equals(JButton.class)) {
				JButton clickedButton = (JButton)e.getSource();
				if (clickedButton.getText().equals("Open File")) {
					openFile = gui.openFile();
				} else if (clickedButton.getText().equals("Convert File")) {
					convertFile();
				}
			}
			
		}
	}
	
	public static void convertFile() {
		FileReader reader = null;
		try {
			reader = new FileReader(openFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int c;
		try {
			while ((c = reader.read()) != -1) {
				System.out.print(String.format("%c", c));
			}
			System.out.println("reached end of file");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

