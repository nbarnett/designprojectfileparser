import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


public class MainGUI {
	JFileChooser chooser;
	JFrame frame;
	MainGUI(ActionListener listener) {
		chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Music Text Files (.txt)", "txt");
		chooser.setFileFilter(filter);
		chooser.setAcceptAllFileFilterUsed(false);
		
		frame = new JFrame("Music Player");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.PAGE_START;
		constraints.insets = new Insets(10, 10, 10, 10);
		
		JTextArea textArea = new JTextArea(30, 40);
		textArea.setWrapStyleWord(false);
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 4;
		constraints.gridheight = 5;
		frame.add(textArea, constraints);
		
		JButton octave = new JButton("Octave +-");
		constraints.gridx = 0;
		constraints.gridy = 6;
		constraints.gridwidth = 2;
		constraints.gridheight = 1;
		frame.add(octave, constraints);
		
		Keyboard keyboard = new Keyboard();
		constraints.gridx = 0;
		constraints.gridy = 8;
		constraints.gridwidth = 4;
		constraints.gridheight = 2;
		frame.add(keyboard, constraints);
		
		JButton download = new JButton("Download");
		constraints.gridx = 5;
		constraints.gridy = 0;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		frame.add(download, constraints);
		
		JButton play = new JButton("Play/Pause");
		constraints.gridx = 5;
		constraints.gridy = 1;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		frame.add(play, constraints);
		
		JLabel styleLabel = new JLabel("Style");
		constraints.gridx = 5;
		constraints.gridy = 2;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		frame.add(styleLabel, constraints);
		
		JButton styleButtons = new JButton("Stacatto | Normal | Slurred");
		constraints.gridx = 5;
		constraints.gridy = 3;
		constraints.gridwidth = 3;
		constraints.gridheight = 1;
		frame.add(styleButtons, constraints);
		
		JButton durationButtons = new JButton("Select Duration");
		constraints.gridx = 5;
		constraints.gridy = 4;
		constraints.gridwidth = 3;
		constraints.gridheight = 4;
		frame.add(durationButtons, constraints);
		
		
		frame.pack();
		frame.setVisible(true);
	}
	
	public File openFile() {
		System.out.println("open file");
		
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			System.out.println("Opening " + file.getName());
			return file;
		} else {
			System.out.println("Open file canceled");
			return null;
		}
	}
}
