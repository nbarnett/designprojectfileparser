#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fileParser.h"

#define TRUE  (1 == 1)
#define FALSE (1 == 0)

int main (int argc, char * argv[]) {

    printf("File Parser\n");
    
    char *inputName;
    char *outputName;
    int shouldFreeOutputName = TRUE;
    
    if (argc >= 2) {
        printf("%s\n", argv[1]);
        inputName = argv[1];
        if (argc >= 3) {
            outputName = argv[2];
            shouldFreeOutputName = FALSE;
        } else {
            outputName = malloc(strlen(inputName));
            strncpy(outputName, inputName, strlen(inputName) - 4);
            strcat(outputName, ".bin");
            
        }
    } else {
        perror("invalid arguments\n");
        return EXIT_FAILURE;
    }
    
    printf("%s\n", outputName);
    
    byte *b = NULL;
    
    int size = parseFile(inputName, &b);
    
    if (b) {
        FILE *output;
        output = fopen(outputName, "wb");
    
        fwrite(b, sizeof(byte), size, output);
    
        fclose(output);

    
        printf("\n");
        for (int i = 0; i < size; i++) {
            printf("%X ", b[i]);
        }
        printf("\n");
    
        if (shouldFreeOutputName) free(outputName);
        free(b);
        b = NULL;
    }
    

    return EXIT_SUCCESS;
}