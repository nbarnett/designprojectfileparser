#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    if (argc >= 2) {
        char *fileName = argv[1];
        
        FILE *fp = fopen(fileName, "rb");
        
        if (fp == 0) {
            perror("Could not open file\n");
        } else {
            char byte[140];
            int j = fread(byte, sizeof(char), 140, fp);
            for (int i = 0; i < j && byte[i] != EOF; i++) {
                printf("%X ", byte[i]);
            }
            printf("\n");
        }
        
        fclose(fp);
    }
    
    return EXIT_SUCCESS;
}