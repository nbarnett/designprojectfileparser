/*
 * DynamicByteArray.c
 * Author: Nick Barnett 13/08/15
 *
 * DynamicByteArray ADT provides a way to dynamically create an array
 * with the ability to add, remove, and insert elements. The dynamic array 
 * can then be converted to a static byte array.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "DynamicByteArray.h"

typedef struct _element {
    byte data;
    struct _element *next;
} element;

typedef struct _dynamicByteArray {
    element *first; 
    element *last;
    int numElements;
} dynamicByteArray;


/*
 * Create a new element having data from the argument b.
 * The initial value of e->next is NULL
 * params:
 *      byte b: The initial data to be stored in the new element
 * return:
 *      returns a pointer to the new element created
 */
element *createElement(byte b) {
    element *e = malloc(sizeof(element));
    e->data = b;
    e->next = NULL;
    
    return e;
}

/*
 * Initialise a new DynamicByteArray ADT
 * return:
 *      returns an empty DynamicByteArray
 */
DynamicByteArray createDynamicByteArray(void) {
    // allocate memory for the new DynamicByteArray
    DynamicByteArray d = malloc(sizeof(dynamicByteArray));
    assert(d != NULL); // make sure the memory was successfully allocated
    
    // initialise the components of the DynamicByteArray struct
    d->first = NULL;
    d->last = NULL;
    d->numElements = 0;
    
    // return the new DynamicByteArray
    return d;
}

/*
 * Gets the number of elements in the DynamicByteArray
 * params:
 *      DynamicByteArray d: the DynamicByteArray to get the count of elements for
 * return:
 *      returns the number of elements in the given DynamicByteArray
 */
int getNumElements(DynamicByteArray d) {
    return d->numElements;
}

/*
 * Adds a new element to the end of the given DynamicByteArray 
 * with the byte passed in with b
 * params:
 *      DynamicByteArray d: the DynamicByteArray to add a byte to
 *      byte b: the byte to add to the DynamicByteArray
 * return:
 *      void
 */
void addByte(DynamicByteArray d, byte b) { 
    // create a new element with the data from b
    element *e = createElement(b);
    
    if (d->first == NULL) {
        // if the DynamicByteArray is empty
        // make the first and last pointers in d point to the new element
        d->first = e;
        d->last = e;
    } else {
        // if the DynamicByteArray is not empty
        // direct the next pointer of the current last element to the new element
        d->last->next = e; 
        // change the last pointer from d to the new element
        d->last = e;
    }
    // Increment the count of the number of elements in the DynamicByteArray
    d->numElements++;
}

/*
 * Inserts an element into the DynamicByteArray at the position given by index
 * params:
 *      DynamicByteArray d: the DynamicByteArray to insert the element into
 *      byte b: the byte to insert
 *      int index: the position in the DynamicByteArray to insert the byte into
 * return:
 *      void
 */
void insertByte(DynamicByteArray d, byte b, int index) {
    // create a new element with the data to insert
    element *new = createElement(b);
    // get a pointer to the first element in d
    element *e = d->first;
    
    if (e == NULL) {
        // if the Array is empty
        // change the first and last pointers in d to the new element
        d->first = new;
        d->last = new;
    } else if (index == 0) {
        // if the insertion is at the start of the Array
        // change the first pointer in d to the new element
        d->first = new;
        // change the next pointer in the new element to the previous first element
        new->next = e;
    } else {
        int i = 1;
        // Loop through the DynamicByteArray until e is the element 
        // before the insertion point, 
        // stopping if the end of the DynamicByteArray is reached
        while (i < index && e->next != NULL) {
            i++;
            e = e->next;
        }
        if (e->next == NULL) {
            // If the insertion is at the end of the Array
            d->last = new;
        }
        // change the next pointer in the new element to the next element in the             
        // DynamicByteArray
        new->next = e->next;
        // change the pointer from the previous element to the new element
        e->next = new;
    }
    
    // increment the element count in the DynamicByteArray
    d->numElements++;
}

/*
 * remove the last element from the DynamicByteArray, returning the element that was
 * removed
 * params:
 *      DynamicByteArray d: the DynamicByteArray to be altered
 * return:
 *      byte: the data from the element that was removed
 */
byte removeLast(DynamicByteArray d) {
    // initialise the return value to 0
    byte lastByte = 0;
    
    if (d->first != NULL) {
        // if the Array is not empty
        // get pointers to the first element in the DynamicByteArray
        element *e = d->first;
        element *next = e->next;
        if (next == NULL) {
            // if the DynamicByteArray has one element
            // change the first and last pointers in d to NULL so that d i empty
            d->first = NULL;
            d->last = NULL;
            // set the return value to the data in the element being removed
            lastByte = e->data;
            // free the memory for the element being removed
            free(e);
            // set the pointer to the freed memory to NULL so it cannot be used again
            e = NULL;
        } else {
            // If the Array has more than one element
            // Loop through the BDA until next is the last element
            while (next->next != NULL) {
                e = next;
                next = next->next;
            }
            // set the pointer from the second last element to NULL
            e->next = NULL;
            // set the DBA last pointer to the second last element
            d->last = e;
            // set the return value to the data from the element being removed
            lastByte = next->data;
            // free the memory from the last element
            free(next);
            // set the pointer to the freed memory to NULL
            next = NULL;
        }
        // decrement the element count in d
        d->numElements--;
    }
    
    return lastByte;
}

/*
 * Converts the DBA to a static byte array
 * params: 
 *      DynamicByteArray d: the DBA to convert
 * return: 
 *      a pointer to the start of the byte array
 */
byte *convertToByteArray(DynamicByteArray d) {
    // allocate memory for the number of bytes in the DBA
    byte *b = malloc(d->numElements * sizeof(byte));
    // get a pointer to the first element in the DBA
    element *e = d->first;
    int i = 0;
    
    // loop through the DBA, assigning each element to the byte array at index i
    while (e != NULL) {
        b[i] = e->data;
        e = e->next;
        i++;
    }
    
    return b;
}

/*
 * Frees all the memory used by the DBA
 * params:
 *      DynamicByteArray d: the DBA to be freed
 * return:
 *      void
 */
void disposeDynamicByteArray(DynamicByteArray d) {
    // get a pointer to the first element in d
    element *e = d->first;
    element *next = NULL;
    
    // loop through the DBA, freeing each element 
    while (e != NULL) {
        next = e->next;
        free(e);
        e = next;
    }
    
    // free the memory used by d
    free(d);
    d = NULL;
}

/*
 * Print the data stored in the DBA to stdout using hex digits
 * params:
 *      DynamicByteArray d: the DBA to print
 * return:
 *      void
 */
void printDynamicByteArray(DynamicByteArray d) {
    element *e = d->first;
    
    while (e != NULL) {
        printf("%X ", e->data);
        e = e->next;
    }
    
    printf("\n");
}
