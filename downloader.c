#include <windows.h>
#include "dpcdefs.h"
#include "dpcutil.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fileParser.h"

void download(unsigned char *byteArray, int len);
int storeByte(char *device, unsigned char byte, int addr);

int main(int argc, char *argv[]) {
    printf("Downloader\n");
    
    if (argc < 2) {
        printf("Required Args: fileName\n");
        return EXIT_FAILURE;
    }
    
    printf("%s\n", argv[1]);
    
    char *fileName = argv[1];
    
    unsigned char *bytes;    
    
    int len = parseFile(fileName, &bytes);
    
    printf("Converted file\n"); 
    for (int i = 0; i < len; i++) {
        printf("%02X ", bytes[i]);
    }
    printf("\n");
    
    download(bytes, len);
    
    free(bytes);
    bytes = NULL;
    
    return EXIT_SUCCESS;
}

#define MAX_NAME_LEN 10
#define TIME_OUT 10
void download(unsigned char *byteArray, int len) {
    ERC erc;
    
    if (DpcInit(&erc)) {        
        //DvmgStartConfigureDevices(GetForegroundWindow(), error);
        
        int devIndex = DvmgGetDefaultDev(&erc);
        char device[MAX_NAME_LEN]; 
        
        DvmgGetDevName(devIndex, device, &erc);
        
        int returnVal = TRUE;
        unsigned char addrL = 0;
        unsigned char addrH = 0;
        unsigned char flag = 0x01;
        
        HANDLE hif;
        
        if (DpcOpenData(&hif, device, &erc, NULL)) {
            for (int i = 0; i < len; i++) {
           
                addrL = (i & 0xFF);
                addrH = ((i >> 8) & 0xFF);
                flag = 0x01;
        
                //printf("addr: %d\n", addr);
        
                DpcPutReg(hif, 0, byteArray[i], &erc, NULL);
                DpcPutReg(hif, 1, addrH, &erc, NULL);
                DpcPutReg(hif, 2, addrL, &erc, NULL);
                DpcPutReg(hif, 3, flag, &erc, NULL);
                
                
                int j = 0;
                while (flag != 0x00 && i < TIME_OUT) {
                    DpcGetReg(hif, 3, &flag, &erc, NULL);
                    j++;
                }
        
                erc = DpcGetFirstError(hif);
                
                if (erc != ercNoError || j == (TIME_OUT-1)) {
                    returnVal = FALSE;
                }
            }
            DpcCloseData(hif, &erc);
        }
        
        if (returnVal) {
            printf("Download Success!\n");
        } else {
            printf("Download Failure!\n");
        }
        
        
        DpcTerm();
    } else {
        printf("init fail\n");
    }

}

/*
#define TIME_OUT 10
int storeByte(char *device, unsigned char byte, int addr) {
    int returnVal = TRUE;
    HANDLE hif;
    ERC erc;
    
    if (DpcOpenData(&hif, device, &erc, NULL)) {
        unsigned char addrL = (addr & 0xFF);
        unsigned char addrH = ((addr >> 8) & 0xFF);
        unsigned char flag = 0x01;
        
        //printf("addr: %d\n", addr);
        
        DpcPutReg(hif, 0, byte, &erc, NULL);
        DpcPutReg(hif, 1, addrH, &erc, NULL);
        DpcPutReg(hif, 2, addrL, &erc, NULL);
        DpcPutReg(hif, 3, flag, &erc, NULL);
        
        int i = 0;
        while (flag != 0x00 && i < TIME_OUT) {
            DpcGetReg(hif, 3, &flag, &erc, NULL);
            i++;
        }
        
        erc = DpcGetFirstError(hif);
        DpcCloseData(hif, &erc);
        
        if (erc != ercNoError || i == (TIME_OUT-1)) {
            returnVal = FALSE;
        }
    }
    return returnVal;
}*/