/*
 * DynamicByteArray.c
 * Author: Nick Barnett 13/08/15
 *
 * DynamicByteArray ADT provides a way to dynamically create an array
 * with the ability to add, remove, and insert elements. The dynamic array 
 * can then be converted to a static byte array.
 *
 */

typedef struct _dynamicByteArray *DynamicByteArray;
typedef unsigned char byte;

/*
 * Initialise a new DynamicByteArray ADT
 * return:
 *      returns an empty DynamicByteArray
 */
DynamicByteArray createDynamicByteArray(void);

/*
 * Gets the number of elements in the DynamicByteArray
 * params:
 *      DynamicByteArray d: the DynamicByteArray to get the count of elements for
 * return:
 *      returns the number of elements in the given DynamicByteArray
 */
int getNumElements(DynamicByteArray d);

/*
 * Adds a new element to the end of the given DynamicByteArray 
 * with the byte passed in with b
 * params:
 *      DynamicByteArray d: the DynamicByteArray to add a byte to
 *      byte b: the byte to add to the DynamicByteArray
 * return:
 *      void
 */
void addByte(DynamicByteArray d, byte b);

/*
 * Converts the DBA to a static byte array
 * params: 
 *      DynamicByteArray d: the DBA to convert
 * return: 
 *      a pointer to the start of the byte array
 */
byte *convertToByteArray(DynamicByteArray d);

/*
 * Frees all the memory used by the DBA
 * params:
 *      DynamicByteArray d: the DBA to be freed
 * return:
 *      void
 */
void disposeDynamicByteArray(DynamicByteArray d);

/*
 * Print the data stored in the DBA to stdout using hex digits
 * params:
 *      DynamicByteArray d: the DBA to print
 * return:
 *      void
 */
void printDynamicByteArray(DynamicByteArray d);

/*
 * Inserts an element into the DynamicByteArray at the position given by index
 * params:
 *      DynamicByteArray d: the DynamicByteArray to insert the element into
 *      byte b: the byte to insert
 *      int index: the position in the DynamicByteArray to insert the byte into
 * return:
 *      void
 */
void insertByte(DynamicByteArray d, byte b, int index);

/*
 * remove the last element from the DynamicByteArray, returning the element that was
 * removed
 * params:
 *      DynamicByteArray d: the DynamicByteArray to be altered
 * return:
 *      byte: the data from the element that was removed
 */
byte removeLast(DynamicByteArray d);