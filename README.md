# File Parser 
Includes all the files required for converting a .txt file that conforms to the [Music File Specification](http://webapps.cse.unsw.edu.au/webcms2/course/index.php?cid=2424) into a .bin file conforming to the [Data Format](https://drive.google.com/open?id=13Aaac3Jvix7d9cHfPlZV33uNW-K5bQCXtIZ-uGO11-8)
## Included Files
This repository contains all the files required to compile the fileParser program:  

* fileParser.c
* DynamicByteArray.h
* DynamicByteArray.c

The fileParser program should be compiled using: `gcc -O -o fileParser fileParser.c DynamicByteArray.c` and run using the command: `./fileParser OdeToJoy.txt`, where   `OdeToJoy.txt` is a sample music file. The output is written to `OdeToJoy.bin`.  
In this repository is also included programs to test `fileParser` and `DynamicByteArray`, as well as `binaryReader`, which reads a binary file and prints the hex representation of each byte.