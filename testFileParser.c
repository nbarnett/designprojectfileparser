#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

char noteValue(char *note);
char durationValue(char *dur);
void convert(char * str);

typedef enum {BPM, STYLE, NOTE} mode;

int main (int argc, char *argv[]) {
    
    printf("Note: %d\n", noteValue("C4"));
    printf("Note: %d\n", noteValue("A5#"));
    printf("Note: %d\n", noteValue("B4b"));
    printf("Note: %d\n", noteValue("g4"));
    printf("Note: %d\n", noteValue("F3"));
    
    printf("\n");
    
    printf("Duration: %d\n", durationValue("b"));
    printf("Duration: %d\n", durationValue("sb"));
    printf("Duration: %d\n", durationValue("dm"));
    printf("Duration: %d\n", durationValue("m"));
    printf("Duration: %d\n", durationValue("dc"));
    printf("Duration: %d\n", durationValue("c"));
    printf("Duration: %d\n", durationValue("dq"));
    printf("Duration: %d\n", durationValue("q"));
    printf("Duration: %d\n", durationValue("t"));
    printf("Duration: %d\n", durationValue("sq"));
    
    printf("\n");
    
    char s[] = "bpm 120";
    convert(s);
    char t[] = "C5# dm";
    convert(t);
    char u[] = "staccato";
    convert(u);
    char v[] = "rest t";
    convert(v);
        
    return EXIT_SUCCESS;
}



char noteValue(char *note) {
    int length = strlen(note);
    char noteName = 0;
    if (length < 2) {
        perror("Invalid Syntax\n");
        return 0;
    } else {
        noteName = note[0];
        noteName = tolower(noteName);
        assert(noteName <= 'g');
        noteName -= 'c';
        if (noteName < 0) noteName = (noteName + 7);
        noteName *= 2;
        if (noteName <= 4) noteName++;
        
        int octave = atoi(note + 1);
        assert(octave >= 3 && octave <= 6);
        noteName += 12 * (octave - 3);
        if (length > 2) {
            char mod = note[2];
            if (mod == '#') noteName++;
            if (mod == 'b') noteName--;
        } 
    }
    return noteName;
}

char durationValue(char *dur) {
    char duration = 4;
    
    if (!strcasecmp(dur, "sq")) {
        duration = 0;
    } else if (!strcasecmp(dur, "t")) {
        duration = 1;
    } else if (!strcasecmp(dur, "q")) {
        duration = 2;
    } else if (!strcasecmp(dur, "dq")) {
        duration = 3;
    } else if (!strcasecmp(dur, "c")) {
        duration = 4;
    } else if (!strcasecmp(dur, "dc")) {
        duration = 5;
    } else if (!strcasecmp(dur, "m")) {
        duration = 6;
    } else if (!strcasecmp(dur, "dm")) {
        duration = 7;
    } else if (!strcasecmp(dur, "sb")) {
        duration = 8;
    } else if (!strcasecmp(dur, "b")) {
        duration = 9;
    }
    
    return duration;
}

void convert(char str[]) {

    int note = 0;
    int duration = 0;
    
    char *token;
    mode str_mode;
    
    token = strtok(str, " ");

    if (!strcasecmp(token, "BPM")) {
        str_mode = BPM;
    } else if (!strcasecmp(token, "NORMAL")) {
        str_mode = STYLE;
        printf("style = normal\n");
    } else if (!strcasecmp(token, "SLURRED")) {
        str_mode = STYLE;
        printf("style = slurred\n");
    } else if (!strcasecmp(token, "STACCATO")) {
        str_mode = STYLE;
        printf("style = staccato\n");
    } else {
        str_mode = NOTE;
        if (!strcasecmp(token, "REST")) {
            note = 0;
        } else {
            note = noteValue(token);
        }
    }
    
    token = strtok(NULL, " ");
    
    if (str_mode == STYLE && token != NULL) {
        perror("Invalid style syntax\n");
    } else if (str_mode == BPM) {
        int bpm = atoi(token);
        assert(bpm >= 60 && bpm <= 200);
        printf("BPM = %d\n", bpm); 
    } else if (str_mode == NOTE) {
        duration = durationValue(token);
        printf("Pitch: %d, Duration: %d\n", note, duration);
    }
    
    if (strtok(NULL, " ") != NULL) {
        perror("Too many arguments\n");
    }
    
}